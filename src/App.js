import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import ActiveProducts from './pages/ActiveProducts';
import AdminDashboard from './pages/AdminDashboard';
import Signup from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminAddProduct from './components/AdminAddProduct';
import AdminEditProduct from './components/AdminEditProduct';
import ProductView from "./components/ProductView";



export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      //User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

        //User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, [])


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/products" element={<ActiveProducts/>} />
            <Route path="/dashboard" element={<AdminDashboard/>} />
            <Route path="/signup" element={<Signup/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/addproduct" element={<AdminAddProduct/>} />
            <Route path="/products/:productId/edit" element={<AdminEditProduct/>}/>
            <Route path="*" element={<Error/>} />
            <Route path="/products/:productId" element={<ProductView/>}/>

          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}


