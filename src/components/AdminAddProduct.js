import { Modal, Form, Button } from "react-bootstrap";
import { useState, useContext, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext.js";
import Swal from 'sweetalert2';


export default function AdminAddProduct(addProductProp) {

	// Pass the closeAddProp function from AdminDash to here.
	const { closeAddProp } = addProductProp;

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

    function createProduct(e){
    	e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
			method:"POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			console.log(user);
			if(data === false){
				console.log(data);
				Swal.fire({
					title: "Duplicate Product Found!",
					icon: "error",
					text: "Product with the same name already exists."
				});

			} else{
				if(data){
					console.log(data);
					setName("");
					setDescription("");
					setPrice("");

					Swal.fire({
						title: "New product added!",
						icon: "success",
						text: `${name} has been successfully added to the products.`
					});

					navigate("/products");

				} else{
					Swal.fire({
						title: "Something went wrong!",
						icon: "error",
						text: "Please try again!"
					});
				}
			}
		});
    }

    useEffect(() => {
	    if(name !== "" && description !== "" && price > 0){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }
	}, [name, description, price]);


	return (
		<>
			<Modal.Header closeButton>
				<Modal.Title>Add New Product</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form onSubmit={ (e)=> createProduct(e)}>
					<Form.Group className="mb-3" controlId="name">
						<Form.Label>Product Name</Form.Label>
						<Form.Control
						type="text"
						placeholder="Enter Product Name"
						value={name}
						onChange={e=>setName(e.target.value)}
						required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="description">
						<Form.Label>Description</Form.Label>
						<Form.Control
						type="text"
						placeholder="Enter Description"
						value={description}
						onChange={e=>setDescription(e.target.value)}
						required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="price">
						<Form.Label>Price</Form.Label>
						<Form.Control
						type="number"
						placeholder="&#8369; 10000"
						value={price}
						onChange={e=>setPrice(e.target.value)}
						required
						/>
					</Form.Group>

					{
						(isActive)
						?
						<Button className="mx-1" type="submit" variant="primary" id="submitBtn">
							Add Product
						</Button>
						:
						<Button className="mx-1" type="submit" variant="primary" id="submitBtn" disabled>
							Add Product
						</Button>
					}

					<Button className="mx-1" variant="secondary" onClick={closeAddProp}>
						Close
					</Button>
				</Form>
			</Modal.Body>
		</>
	)
}
