import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { useContext } from 'react';

import UserContext from '../UserContext';


export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (

    <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">

    <Container>
      <Navbar.Brand as={NavLink} to="/">
        <ion-icon name="storefront-outline"></ion-icon>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link className="mx-1" as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link className="mx-1" as={NavLink} to="/products">Products</Nav.Link>
        {
          (user.isAdmin) ?
          <>
            <Nav.Link className="mx-1" as={NavLink} to="/dashboard">Dashboard</Nav.Link>
          </>
          :
            <></>
        }
        </Nav>

        <Nav>
        { 
          (user.id !== null) ?
          <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        :
          <>
            <Nav.Link className="mx-1" as={NavLink} to="/signup">Sign up</Nav.Link>
            <Nav.Link disabled> | </Nav.Link>
            <Nav.Link className="mx-1" as={NavLink} to="/login">Log in</Nav.Link>
          </>
        }
        </Nav>
          
      </Navbar.Collapse>
    </Container>
    </Navbar>
  );

}

