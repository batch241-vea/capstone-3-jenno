import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function AdminUpdateProduct() {

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

	function editProduct(e) {

	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    price: price,
			    isActive: isActive
			})
	    })
	    .then(response => response.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Product Succesfully Updated",
	    		    icon: "success",
	    		    text: `The product is now updated`
	    		});

	    		navigate("/products");
	    	}

	    	else{
	    		Swal.fire({
	    		    title: "Modify first to update!",
	    		    icon: "error",
	    		    text: `Try again.`
	    		});
	    	}

	    })

	    setName('');
	    setDescription('');
	    setPrice(0);
	}


	useEffect(() => {

        if(name !== "" && description !== "" && price > 0 ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

    useEffect(()=> {

    	console.log(productId);

    	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    	.then(response => response.json())
    	.then(data => {

    		console.log(data);

    		setName(data.name);
    		setDescription(data.description);
    		setPrice(data.price);
    	});

    }, [productId]);

    return (
    	user.isAdmin
    	?
			<>
		    	<center><h1 className="my-5 text-center" id="editProd">Edit Product</h1></center>
		        <Form onSubmit={(e) => editProduct(e)} id="editTbl">
		        	<Form.Group controlId="name" className="mb-2">
		                <strong><Form.Label className="mx-2 my-2">Product Name</Form.Label></strong>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Product Name" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-2">
		                <strong><Form.Label className="mx-2 my-2">Product Description</Form.Label></strong>
		                <Form.Control
		                	type="text"
			                placeholder="Enter Product Description" 
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="price" className="mb-2">
		                <strong><Form.Label className="mx-2 my-2">Product Price</Form.Label></strong>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Product Price" 
			                value = {price}
			                onChange={e => setPrice(e.target.value)}
			                required
		                />
		            </Form.Group>
		            <center>
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/dashboard" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
	        	    </center>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}
