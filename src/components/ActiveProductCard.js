import { Link } from 'react-router-dom';

import { Button, Card, CardGroup } from 'react-bootstrap';


export default function ActiveProductCard({productProps}){

	const { name, description, price, _id } = productProps;

	return(
    <center>
    <CardGroup  style={{ width: '30rem'}}>
	<Card className="my-2" id="Card">
      
        <center><Card.Title className="mt-2">{name}</Card.Title></center>

        <Card.Body id="cardBody">
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>

        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}.00</Card.Text>
        </Card.Body>
        <Card.Footer>
            <center><Button style={{ width: '10rem' }} className="bg-info text-dark" as={Link} to={`/products/${_id}`}>Details</Button></center>
        </Card.Footer>

    </Card>
    </CardGroup></center>
    
	)
}