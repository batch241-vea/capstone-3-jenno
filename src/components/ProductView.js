import React, { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext.js";
import Swal from "sweetalert2";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription]= useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);

	const [isActive, setIsActive] = useState(false);

	function order(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
			method:'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				productName: name,
				quantity: quantity,
				totalAmount: price * quantity	
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data){
				console.log(data);

				Swal.fire({
					title: `Product ordered!`,
					icon: "success",
					text: `${name} has been successfully ordered!`
				});
				navigate("/products");

			} else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				});
			}
		});
    }

	useEffect(() => {
		console.log(user.id);
		console.log(productId);
		console.log(name);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setTotalAmount(data.totalAmount);

		});

	    if ((price * quantity) > 0) {
	        setIsActive(true);

	    } else {
	        setIsActive(false);
	    }
	    
	}, [user.id, productId, name, quantity, price]);

	
	return (
	
		(user.id !== null) ?

			(user.isAdmin) ?

			<Container className="mt-5">
				<Row>
					<Col lg={{span: 6, offset: 3}}>
					    <center><Card id="Card">
					    <Card.Title className="pt-3">{name}</Card.Title>
					    <Card.Body id="cardBody">
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text> 			        	
					    </Card.Body>
					    <Card.Footer>
					    	<center><Button variant="secondary" as={ Link } to={`/products/${productId}/edit`} size="sm" className="my-1 w-25">Edit</Button></center>
					    </Card.Footer>
					    </Card></center>
				    </Col>
			    </Row>
		    </Container>
		    
		    :
		    
		    <Container className="mt-5">
				<Row>
					<Col lg={{span: 6, offset: 3}}>
					    <center><Card id="Card">
					    <Card.Title className="pt-3">{name}</Card.Title>
					    <Card.Body id="cardBody">
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<Form>
					        	<Form.Group controlId="quantity" className="mb-3 w-25">
									<Form.Control 
									type="number"
									value={quantity}
									placeholder="Quantity" 
									onChange={e=>setQuantity(e.target.value)}
									required
									/>
								</Form.Group>
								<Card.Subtitle>Total Amount:</Card.Subtitle>
					        	<Card.Text value={totalAmount}>&#8369; {price * quantity}</Card.Text>				 
					        </Form>
					    </Card.Body>
					    <Card.Footer>
					      	{
					        	(isActive) ?
					        		
					        	<center><Button variant="info" onClick={(e) => order(e)}>Order</Button></center>
					        		
					        	:
					        		
					        	<center><Button variant="info" onClick={(e) => order(e)} disabled>Order</Button></center>
					
					        }
					    </Card.Footer>
					    </Card></center>
				    </Col>
			    </Row>
		    </Container>    
		
		:

			<Container className="mt-5">
				<Row>
					<Col lg={{span: 6, offset: 3}}>
					    <center><Card id="Card">
					    <Card.Title className="pt-3">{name}</Card.Title>
					    <Card.Body id="cardBody">
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					    </Card.Body>
					    <Card.Footer>
					    	<center><Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button></center>
					     </Card.Footer>
					    </Card></center>
				    </Col>
			    </Row>
		    </Container>
	)		

}
