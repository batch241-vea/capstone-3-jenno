import { Navigate } from 'react-router-dom';

import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

export default function Logout(){

	const { unsetUser, setUser } = useContext(UserContext);
	//localStorage.clear();
	//clears the localStorage(user's information)
	unsetUser();

	useEffect(() => {
		// sets the user state back to its original value
		setUser({id: null})
	})

	return (
		<Navigate to="/login"/>
	)
}
