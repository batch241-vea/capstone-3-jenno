import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';

import { NavLink } from 'react-router-dom';

export default function Error() {

    return (
        <center>
        <Container>
            <Row>
                <Col className="p-5">
                    <h1>Error 404 - Page Not Found</h1>
                    <p>The page you are looking for cannot be found.</p>

                    <Button variant="primary" as={NavLink} to='/'>Go back</Button>
                </Col>
            </Row>
        </Container>
        </center>
    )

}