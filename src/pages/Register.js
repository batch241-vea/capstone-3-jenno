import { Button, Form, Row, Col } from 'react-bootstrap';

import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Register(){

    const {user} = useContext(UserContext);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const navigate = useNavigate();

    const [isActive, setIsActive] = useState(false);
    
    
    function registerUser() {
        
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(data){
                Swal.fire({
                    title: "Registration successful!",
                    icon: "success",
                    text: "Log in to continue"              
                })
                setFirstName('');
                setLastName('');
                setEmail('');
                setPassword1('');
                setPassword2('');
                setIsActive(false);
                navigate("/login");

            } else {
                Swal.fire({
                    title: "Registration failed!",
                    icon: "error",
                    text: "Please try again"
                })

            }
            
        })
        
    }

    const checkEmail = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({email})
        })
        .then(res => res.json())
        .then(data => {
            if (data){
                Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide a different email!"
            })

            } else {
                registerUser();
            }
        })
    }

        useEffect(()=> {
            if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '')&&(password1 === password2)){
                setIsActive(true);
            } else{
                setIsActive(false);
            }
        }, [firstName, lastName, email, password1, password2]);
    

        return (
            (user.id !== null) ?
            <Navigate to="/login"/>
            :
            <Row className="mt-3">
            <Col md={{ span: 6, offset: 3 }} className="bg-light" id="signup">
            <Form className="pb-4" onSubmit={(e) => checkEmail(e)}><br/>
            <center><h1>Sign up</h1><br/></center>

                <Form.Group className="mb-2" controlId="userFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="First Name" 
                        value = {firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group className="mb-2" controlId="userLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Last Name" 
                        value = {lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group className="mb-2" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Email" 
                        value = {email}
                        onChange={e => setEmail(e.target.value)}
                        required
                        />
                </Form.Group>

                <Form.Group className="mb-2" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                />
                </Form.Group>

                <Form.Group className="mb-2" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value = {password2}
                    onChange = {e => setPassword2(e.target.value)} 
                    required
                />
                </Form.Group><br/>

                { isActive ?
                <center><Button variant="secondary" type="submit" id="registerBtn">
                Register
                </Button></center>
                :
                <center><Button variant="secondary" type="submit" id="registerBtn" disabled>
                Register
                </Button></center>
                }
                
            </Form>
            </Col>
            </Row>
        );
}