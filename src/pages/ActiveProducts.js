import { useState, useEffect } from 'react';

import ActiveProductCard from '../components/ActiveProductCard';


export default function ActiveProducts(){

	const [products, setProducts] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {


				return (

					<ActiveProductCard key = {product.id} productProps = {product} />
					
					)
			}))
		})
	}, [])


	return(
		<>
		<center><h1 id="products" className='pt-3 mb-4'>AVAILABLE PRODUCTS</h1></center>
		{products}

		</>
	)
}