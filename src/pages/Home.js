import React from 'react';
import { Button } from 'react-bootstrap';
import UserContext from "../UserContext.js";
import { NavLink } from 'react-router-dom';
import { useContext } from 'react';


export default function Home() {

	const { user } = useContext(UserContext);

	console.log(user);
	
	return(

    	<center>
    	<div id="divMain">
    	    <h1 id="ssMain">IS COOL SOUP LIES?</h1>
      	</div>
 
      	    <Button id="homeBtn" as={NavLink} to="/products">
        		<h3 id="ordernow" class="">Order Now!</h3>
      		</Button>
    	</center>
	)	

}